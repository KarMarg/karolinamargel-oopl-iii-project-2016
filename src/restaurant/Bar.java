package restaurant;

public class Bar extends Restaurant implements Barman{
	public void cleaning() {
		setClean(true);
		System.out.println("The Bar is cleaned.");
	}
	public void order() {
		System.out.println("The order is realized at the Bar.");
	}
	@Override
	public void realizeOrder() {
		System.out.println("Barman realizes the order.");
	}
	@Override
	public void cleanBar() {
		setClean(true);
		System.out.println("Barman cleans the Bar.");
	}
	
}

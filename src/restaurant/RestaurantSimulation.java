package restaurant;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Iterator;

public class RestaurantSimulation {

	public static void main(String[] args) throws CustomException {
		Restaurant restaurant = new Restaurant();
		Hall hall = new Hall();
		Restaurant kitchen = new Kitchen();
		Restaurant bar = new Bar();
		
		int numberOfTables = 20;
		restaurant.open();
		hall.addTables(numberOfTables);
		
		BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
		
		boolean done = false;
		
		do {
			int choice = 0;
			System.out.println("What you want to do: \n"
					+ "1.Close Restaurant\n"
					+ "2.Check the cleanliness\n"
					+ "3.Clean the restaurant\n"
					+ "4.Check which table is free.\n"
					+ "5.Receive new customer\n"
					+ "6.Clean the table.");
			
			try {
				choice = Integer.parseInt(reader.readLine().trim());
			} catch (IOException e) {
				e.printStackTrace();
			}
			
			if(choice==1){
				restaurant.close();
				done=true;
			}
			else if(choice==2){
				if(restaurant.getClean()==true || (bar.getClean()==true && kitchen.getClean()==true)){
					done=false;
					System.out.println("Restaurant is clean.\n");
				}
				else if(bar.getClean()==true && kitchen.getClean()==false){
					done=false;
					System.out.println("Bar is clean. Kitchen is not clean.");
				}
				else if(bar.getClean()==false && kitchen.getClean()==true){
					done=false;
					System.out.println("Kitchen is clean. Bar is not clean.");
				}
				else{
					done=false;
					System.out.println("Restaurant is not clean.\n");
				}
			}
			else if(choice==3){
				restaurant.cleaning();
				done=false;
			}
			else if(choice==4){
				Iterator<Table> tableIterator = hall.getTables().iterator();
				Table t = null;
				System.out.print("Free tables: ");
				
				while(tableIterator.hasNext()){
					t = tableIterator.next();
					
					if(t.getTaken() == false){
						System.out.print("  " + t.getNumberOfTable() + " ");
					}
				}
				System.out.println("");
			}
			else if(choice==5){
				System.out.println("The Customer enter the Restaurant.");
				
				boolean done4 = false;
				int tableNumber = 0;
				String customerName = "";
				
				do {
					System.out.println("Enter the name of the customer.");
					
					try {
						customerName = String.valueOf(reader.readLine().trim());
					} catch (IOException e) {
						e.printStackTrace();
					}
					
					System.out.println("Choose the table number from 1 to 20: ");
					try {
						tableNumber = Integer.parseInt(reader.readLine().trim());
					} catch (IOException e) {
						e.printStackTrace();
					}
					
					if(tableNumber>numberOfTables){
						throw new CustomException("Wrong table number.");
					}
					
					if(hall.getTables().get(tableNumber).getTaken() == false){
						System.out.println("The table number " + tableNumber + " is free. " + customerName + " set the table.");
						
						hall.assign(tableNumber, customerName);
						hall.getTables().get(tableNumber-1).setTaken(true);
						hall.getTables().get(tableNumber-1).order();
						bar.realizeOrder();
						kitchen.realizeOrder();
						hall.getTables().get(tableNumber-1).deliverOrder();
						bar.cleaning();
						kitchen.cleaning();
						System.out.println("The Customer is eating.\n");
						done4=true;
					}
					else if(hall.getTables().get(tableNumber).getTaken() == true){
						System.out.println("The table number " + tableNumber + " is taken. You have to choose another table.");
						done4=false;
					}
				} while(done4==false);
				//done=false;
			}
			
			else if(choice==6){
				System.out.println("Which table do you want to clean? (From 1 to " + numberOfTables + ")");
				
				int tableNum = 0;
			
				try {
					tableNum = Integer.parseInt(reader.readLine().trim());
				} catch (IOException e) {
					e.printStackTrace();
				}
				
				if(tableNum>20){
					throw new CustomException("Wrong table number.");
				}
				
				hall.getTables().get(tableNum-1).clean();
				hall.getTables().get(tableNum-1).setTaken(false);
				System.out.println("");
			}
			else{
				throw new CustomException("Wrong number.");
				
			}
		} while(done==false);

	}

}

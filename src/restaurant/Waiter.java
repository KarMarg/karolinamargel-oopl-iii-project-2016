package restaurant;

public interface Waiter {
	void takeOrder();
	void deliverOrder();
	void clean();

}

package restaurant;

public interface Barman {
	void realizeOrder();
	void cleanBar();
}

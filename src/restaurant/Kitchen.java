package restaurant;

public class Kitchen extends Restaurant implements Chef {
	public void cleaning() {
		setClean(true);
		System.out.println("The Kitchen is cleaned.");
	}
	public void order() {
		System.out.println("The order is realized in the Kitchen.");
	}
	@Override
	public void realizeOrder() {
		System.out.println("Chef realizes the order.");
	}
	@Override
	public void cleanKitchen() {
		setClean(true);
		System.out.println("Chef cleans the Kitchen.");
	}
	
}

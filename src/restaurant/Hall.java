package restaurant;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class Hall extends Restaurant {
	private final List<Table> tables = new ArrayList<Table>();
	private final Map <Integer,Customer> customtab = new HashMap<Integer,Customer>();
	
	public void cleaning() {
		System.out.println("The Hall is cleaned.");
	}
	
	public void addTables(int numberOfTables){
		for(int i=1; i<=numberOfTables; i++){
			tables.add(new Table(i));
		}	
	}
	
	public void assign(int numberOfTable, String name){
		customtab.put(numberOfTable, new Customer(name));
		System.out.println(name + " was assigned to the table number " + numberOfTable);
	}
	
	public List<Table> getTables() {
		return tables;
	}

	public Map <Integer,Customer> getCustomtab() {
		return customtab;
	}
	
}

package restaurant;

public class Restaurant {
	private String name = "Bon Appetit";
	private boolean isOpen = false;
	private boolean isClean = false;
	
	public void open() {
		setIsOpen(true);
		System.out.println(name + " Restaurant is opened.");
	}
	public void close() {
		setIsOpen(false);
		System.out.println("The Restaurant is closed.");
	}
	
	public void cleaning() {
		setClean(true);
		System.out.println("The Restaurant is cleaned.");
	}
	public void order() {
		System.out.println("The order is realized in the Restaurant.");
	}
	
	public boolean getIsOpen() {
		return isOpen;
	}
	public void setIsOpen(boolean isOpen) {
		this.isOpen = isOpen;
	}
	
	public boolean getClean() {
		return isClean;
	}
	public void setClean(boolean isClean) {
		this.isClean = isClean;
	}
	
	public void realizeOrder(){}
//	public void cleanBar(){}
//	public void cleanKitchen(){}
//	public void addTables(){}
}

package restaurant;

public class Table extends Hall implements Waiter {
	private int numberOfTable;
	private boolean isTaken = false;
	
	public Table(){
	}
	
	public Table(int numberOfTable){
		this.numberOfTable = numberOfTable;
	}

	public int getNumberOfTable() {
		return numberOfTable;
	}

	public void setNumberOfTable(int numberOfTable) {
		this.numberOfTable = numberOfTable;
	}
	
	public void cleaning() {
		System.out.print("The Table number ");
		System.out.print(getNumberOfTable());
		System.out.println(" is cleaned.");
	}
	
	public void order() {
		System.out.print("The order is taken from Table number ");
		System.out.println(getNumberOfTable());
	}
	
	public void orderDelivery() {
		System.out.print("The order is delivered to the Table number ");
		System.out.println(getNumberOfTable());
	}

	@Override
	public void takeOrder() {
		System.out.print("Waiter takes the order from the Table number ");
		System.out.println(getNumberOfTable());
	}

	@Override
	public void deliverOrder() {
		System.out.print("Waiter delivers the order to the Table number ");
		System.out.println(getNumberOfTable());
	}

	@Override
	public void clean() {
		System.out.print("Waiter cleans the Table number ");
		System.out.println(getNumberOfTable());
	}

	public boolean getTaken() {
		return isTaken;
	}

	public void setTaken(boolean isTaken) {
		this.isTaken = isTaken;
	}

}

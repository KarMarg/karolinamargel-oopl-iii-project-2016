package restaurant;

public class Customer extends Hall {
	private String name;
	
	public Customer(){}
	public Customer(String name){
		this.name = name;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
}

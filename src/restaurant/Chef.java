package restaurant;

public interface Chef {
	void realizeOrder();
	void cleanKitchen();
}
